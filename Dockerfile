FROM centos/python-38-centos7

USER root

ENV USER_HOME /home/centos

RUN yum update -y --quiet \
    && pip install -U flask \ 
    && pip install -U flask-jsonpify \
    && pip install -U flask-restful \
    && groupadd centos \
    && useradd centos -g centos -c 'Centos User' -d $USER_HOME
    
WORKDIR $USER_HOME/python_api

COPY ./python-api.py python-api.py

USER centos

ENTRYPOINT ["python3", "python-api.py"]
